var phantom = require('node-phantom'),
	child = require('child_process'),
	url	= require('url'),
	fs	= require('fs'),
	path = require('path'),
	log = console.log;

var Grabber = (function() {

	/**
	 * [Grabber instanse attributes]
	 * @type {Array}
	 *
	 * "target"  - url of resource, that will be grabbed
	 * 
	 * "include" - url of js file, that will include to "target" page
	 * 
	 * "evaluate" - function, that will be eval in "browser console" context
	 * 
	 * "process"  - function, that will be eval with result of "evaluate"
	 * 
	 */
	var attributes = ["target"  , 
                      "include" , 
                      "inject"  , 
                      "evaluate", 
                      "process"];

	function Grabber(opts) {
		if (!(this instanceof Grabber)) {
			return new Grabber(opts);
		}

		for (var i = 0, count = attributes.length; i < count; i++) {
			if (opts && opts[attributes[i]]) {
				this[attributes[i]] = opts[attributes[i]]; 
			}
		}

		log(this);
	}

	Grabber.prototype.start = function() {
		var self = this;
		phantom.create(function(err, ph) {
			self.ph = ph;
			log_err("phantom.create", err);
			ph.createPage(function(err, page) {
				log_err("ph.createPage", err);
				self.page = page;
				return page.open(self.target, function (err, status) {
					log_err("page.open", err);
					if (self.include) {
						log("Use inject instead of include");
						self.inject = resolve_js(self.include);
					}
					if (self.inject) {
						page.injectJs(self.inject, function(err) {
							log_err("page.injectJs", err);
							self.grab(page);

						});
					} else{
						self.grab(page);
					}
				});
			});
		});
	};

	Grabber.prototype.grab = function(page) {
		page.evaluate(this.evaluate, this.process);
		this.ph.exit();
	};

	Grabber.prototype.render = function(name) {
		var r_name;
		if (typeof name === 'string') {
			r_name = name;
		} else if (typeof name === 'function') {
			r_name = name();
		} else {
			r_name = path.join(__dirname, "/", Date.now().toString() + ".png");
		}
		log(r_name);
		this.page.render(r_name);
	};

	/**
	 * 	Private
	 */

	/**
	 * download via wget js if it isnt present
	 * @param  {[String]} js_url [url of js file]
	 * @return {[String]}        [path of js file]
	 */
	var resolve_js = function(js_url) {
		var js_file_name = url.parse(js_url).pathname.split('/').pop();
		if (!fs.existsSync(path.resolve(js_file_name))) {
			child.spawnSync('wget', [js_url], {});
			log("download", js_file_name);
		}
		return js_file_name;
	};

	/**
	 * Log that error occured
	 * @param  {[type]} section [Where called, simple text]
	 * @param  {[type]} err     [Error object]
	 */
	var log_err = function(section, err) {
		if (err) {
			log(section + "err\n" + err);
		}
	};

	return Grabber;
})();



module.exports = Grabber;